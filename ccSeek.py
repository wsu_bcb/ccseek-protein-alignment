"""
This GUI program is designed to visually align protein sequences 
    and discover motifs shared by a group of proteins

Users may want to open a sequence file first
The current version supports input file formats: .fasta and .txt

For .fasta files,
    each sequence begins with a line of sequence ID like '>ID'
    after the ID line, one or more lines represent the sequence itself

For .txt files,
    each line stands for a sequence
    there is no sequence ID's

The program will display the alignment with colored circles on the screen
Users are able to interactively align the sequences

By default, the program will align the sequences by amino acid identity
Users may choose the property they want to view and align

Users have the options to configure properties, to define different set of 
    categories for a certain property, as well as to change the color settings

To save files, users may save the alignment images as various formats, 
    save the alignment sequences to .fasta or .txt files,
    and save the active alignment settings for record and future use

-------------------------------------------------------------------------------
Author:					Roben Yunbing Tan
Copyright:				Washington State University
Date:					May 13, 2012
License:				GNU General Public License version 3

"""

import sys

# Use classes from 3rd-party package PyQt4
from PyQt4 import QtGui, QtCore

# import local module
from form_main import *


def main():
    """application starts here"""
    app = QtGui.QApplication(sys.argv)
    app.windowList = []
    
    # setup the splash screen when the program starts
    pixmap = QtGui.QPixmap('./splash.png')
    if not pixmap.isNull():
        pixmap = pixmap.scaled(400, 300)
        splashScreen = QtGui.QSplashScreen(pixmap, QtCore.Qt.WindowStaysOnTopHint)
        splashScreen.show()
        app.processEvents()
    
    mainWindow = RMainWindow(app, '')
    app.windowList.append(mainWindow)
    mainWindow.show()
    
    if not pixmap.isNull():
        splashScreen.finish(mainWindow)
    
    sys.exit(app.exec_())


if __name__ == '__main__':
    main()
