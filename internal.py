"""
This file constructs all internal settings for the program
If there is no default settings from a setting file, 
    these internal settings will be used to guarantee the program's workability

Advanced users may modify this file to change the property values or settings
Also, advanced users may introduce new properties or delete properties

One must keep in mind the data consistency if he/she modifies the file

"""

from PyQt4 import QtGui

AA_LIST = 'ACDEFGHIKLMNPQRSTVWY'

def constructAAPropertiesInternal():
    """
    The function defines the aa properties
    Input: None
    Return: AA properties dict
    """
    aaProperties = {}
    for aa in AA_LIST:
        aaProperties[aa] = {}
        aaProperties[aa]['size'] = None
        aaProperties[aa]['charge'] = None
        aaProperties[aa]['polarity'] = None
        aaProperties[aa]['hydropathy'] = None
    
    aaProperties['A']['size'] = 89.09
    aaProperties['A']['charge'] = 0
    aaProperties['A']['polarity'] = 0
    aaProperties['A']['hydropathy'] = 1.8
    
    aaProperties['C']['size'] = 121.15
    aaProperties['C']['charge'] = 0
    aaProperties['C']['polarity'] = 1
    aaProperties['C']['hydropathy'] = 2.5
    
    aaProperties['D']['size'] = 133.10
    aaProperties['D']['charge'] = -1
    aaProperties['D']['polarity'] = 1
    aaProperties['D']['hydropathy'] = -3.5
    
    aaProperties['E']['size'] = 147.13
    aaProperties['E']['charge'] = -1
    aaProperties['E']['polarity'] = 1
    aaProperties['E']['hydropathy'] = -3.5
    
    aaProperties['F']['size'] = 165.19
    aaProperties['F']['charge'] = 0
    aaProperties['F']['polarity'] = 0
    aaProperties['F']['hydropathy'] = 2.8
    
    aaProperties['G']['size'] = 75.07
    aaProperties['G']['charge'] = 0
    aaProperties['G']['polarity'] = 0
    aaProperties['G']['hydropathy'] =-0.4
    
    aaProperties['H']['size'] = 155.16
    aaProperties['H']['charge'] = 0
    aaProperties['H']['polarity'] = 1
    aaProperties['H']['hydropathy'] = -3.2
    
    aaProperties['I']['size'] = 131.17
    aaProperties['I']['charge'] = 0
    aaProperties['I']['polarity'] = 0
    aaProperties['I']['hydropathy'] = 4.5
    
    aaProperties['K']['size'] = 146.19
    aaProperties['K']['charge'] = 1
    aaProperties['K']['polarity'] = 1
    aaProperties['K']['hydropathy'] = -3.9
    
    aaProperties['L']['size'] = 131.17
    aaProperties['L']['charge'] = 0
    aaProperties['L']['polarity'] = 0
    aaProperties['L']['hydropathy'] = 3.8
    
    aaProperties['M']['size'] = 149.21
    aaProperties['M']['charge'] = 0
    aaProperties['M']['polarity'] = 0
    aaProperties['M']['hydropathy'] = 1.9
    
    aaProperties['N']['size'] = 132.12
    aaProperties['N']['charge'] = 0
    aaProperties['N']['polarity'] = 1
    aaProperties['N']['hydropathy'] = -3.5
    
    aaProperties['P']['size'] = 115.13
    aaProperties['P']['charge'] = 0
    aaProperties['P']['polarity'] = 0
    aaProperties['P']['hydropathy'] = -1.6
    
    aaProperties['Q']['size'] = 146.15
    aaProperties['Q']['charge'] = 0
    aaProperties['Q']['polarity'] = 1
    aaProperties['Q']['hydropathy'] = -3.5
    
    aaProperties['R']['size'] = 174.20
    aaProperties['R']['charge'] = 1
    aaProperties['R']['polarity'] = 1
    aaProperties['R']['hydropathy'] = -4.5
    
    aaProperties['S']['size'] = 105.09
    aaProperties['S']['charge'] = 0
    aaProperties['S']['polarity'] = 1
    aaProperties['S']['hydropathy'] = -0.8
    
    aaProperties['T']['size'] = 119.12
    aaProperties['T']['charge'] = 0
    aaProperties['T']['polarity'] = 1
    aaProperties['T']['hydropathy'] = -0.7
    
    aaProperties['V']['size'] = 117.15
    aaProperties['V']['charge'] = 0
    aaProperties['V']['polarity'] = 0
    aaProperties['V']['hydropathy'] = 4.2
    
    aaProperties['W']['size'] = 204.24
    aaProperties['W']['charge'] = 0
    aaProperties['W']['polarity'] = 0
    aaProperties['W']['hydropathy'] = -0.9
    
    aaProperties['Y']['size'] = 181.19
    aaProperties['Y']['charge'] = 0
    aaProperties['Y']['polarity'] = 1
    aaProperties['Y']['hydropathy'] = -1.3
    
    return aaProperties


def constructAASettingsInternal():
    
    aaSettings = {}
    aaSettings['size'] = {}
    aaSettings['charge'] = {}
    aaSettings['polarity'] = {}
    aaSettings['hydropathy'] = {}
    
    aaSettings['size']['small'] = (0.0, 125.0)
    aaSettings['size']['medium'] = (125.0, 165.0)
    aaSettings['size']['large'] = (165.0, float('inf'))

    aaSettings['charge']['negative'] = (float('-inf'), -0.1)
    aaSettings['charge']['neutral'] = (-0.1, 0.1)
    aaSettings['charge']['positive'] = (0.1, float('inf'))
    
    aaSettings['polarity']['nonpolar'] = (0.0, 1.0)
    aaSettings['polarity']['polar'] = (1.0, float('inf'))
    
    aaSettings['hydropathy']['hydrophilic'] = (float('-inf'), -0.4)
    aaSettings['hydropathy']['neutral'] = (-0.4, 0.5)
    aaSettings['hydropathy']['hydrophobic'] = (0.5, float('inf'))
    
    return aaSettings


def constructPropertyColorsInternal():
    
    propertyColors = {}
    propertyColors['identity'] = {}
    propertyColors['size'] = {}
    propertyColors['charge'] = {}
    propertyColors['polarity'] = {}
    propertyColors['hydropathy'] = {}
    
    propertyColors['identity']['A'] = QtGui.QColor(0x7700FF)
    propertyColors['identity']['C'] = QtGui.QColor(0x00FFFF)
    propertyColors['identity']['D'] = QtGui.QColor(0xAAAAAA)
    propertyColors['identity']['E'] = QtGui.QColor(0x777777)
    propertyColors['identity']['F'] = QtGui.QColor(0xFF00FF)
    propertyColors['identity']['G'] = QtGui.QColor(0x7700AA)
    propertyColors['identity']['H'] = QtGui.QColor(0xFFFF00)
    propertyColors['identity']['I'] = QtGui.QColor(0xFF0077)
    propertyColors['identity']['K'] = QtGui.QColor(0xFFDD55)
    propertyColors['identity']['L'] = QtGui.QColor(0xFF77BB)
    propertyColors['identity']['M'] = QtGui.QColor(0xBB77BB)
    propertyColors['identity']['N'] = QtGui.QColor(0x77BBFF)
    propertyColors['identity']['P'] = QtGui.QColor(0xBB00FF)
    propertyColors['identity']['Q'] = QtGui.QColor(0x00FF77)
    propertyColors['identity']['R'] = QtGui.QColor(0xFF8800)
    propertyColors['identity']['S'] = QtGui.QColor(0x0000BB)
    propertyColors['identity']['T'] = QtGui.QColor(0x0077FF)
    propertyColors['identity']['V'] = QtGui.QColor(0xBB0077)
    propertyColors['identity']['W'] = QtGui.QColor(0xBB77FF)
    propertyColors['identity']['Y'] = QtGui.QColor(0x77FFBB)
    
    propertyColors['size']['small'] = QtGui.QColor(0xFFFF77)
    propertyColors['size']['medium'] = QtGui.QColor(0xFF9900)
    propertyColors['size']['large'] = QtGui.QColor(0xFF0000)
    propertyColors['size']['undefined'] = QtGui.QColor(0xFFFFFF)
    
    propertyColors['charge']['negative'] = QtGui.QColor(0x000000)
    propertyColors['charge']['neutral'] = QtGui.QColor(0xCCCCCC)
    propertyColors['charge']['positive'] = QtGui.QColor(0xFF8800)
    propertyColors['charge']['undefined'] = QtGui.QColor(0xFFFFFF)
    
    propertyColors['polarity']['nonpolar'] = QtGui.QColor(0x00FF00)
    propertyColors['polarity']['polar'] = QtGui.QColor(0xFFFF00)
    propertyColors['polarity']['undefined'] = QtGui.QColor(0xFFFFFF)
    
    propertyColors['hydropathy']['hydrophilic'] = QtGui.QColor(0x0000FF)
    propertyColors['hydropathy']['neutral'] = QtGui.QColor(0xCCCCCC)
    propertyColors['hydropathy']['hydrophobic'] = QtGui.QColor(0xFF0000)
    propertyColors['hydropathy']['undefined'] = QtGui.QColor(0xFFFFFF)
    
    return propertyColors


def constructPlotParametersInternal():
    
    plotParameters = {}
    plotParameters['ball radius'] = 8
    plotParameters['ball spacing'] = 2
    plotParameters['top distance'] = 15
    plotParameters['left distance'] = 15
    
    return plotParameters
